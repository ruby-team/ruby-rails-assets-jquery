# rails-assets-jquery

> The Bower package inside a gem

This gem was automatically generated.

## Usage

Add this source block to your `Gemfile`:

```ruby
source "https://gems.diasporafoundation.org" do
  gem "rails-assets-jquery"
end

```

Then, import the asset using Sprockets’ `require` directive:

```js
//= require "jquery"
```
